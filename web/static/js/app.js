let suuid = $('#suuid').val();

let config = {
  iceServers: [{
    urls: ["stun:stun.l.google.com:19302"]
  }]
};
parent = document.getElementById('remoteVideos')
videoElement = document.getElementById('video')

// var videoElement = document.createElement('video')
var mediaStream = new MediaStream()
mediaStream.onaddtrack = event=>{
  if(videoElement.played)
    videoElement.load()
    else videoElement.play()
}
videoElement.muted = true
videoElement.autoplay = true
videoElement.controls = true
videoElement.width = parent.offsetWidth
videoElement.height = videoElement.width*9/16
videoElement.srcObject = mediaStream
// parent.appendChild(videoElement)



const pc = new RTCPeerConnection(config);
pc.onnegotiationneeded = handleNegotiationNeededEvent;

let log = msg => {
   console.log(msg);
}
pc.onicecandidate
pc.ontrack = function(event) {
  let date = new Date()
  log(event.streams[0] + ' track is delivered'+event.track+date.getSeconds()+"."+date.getMilliseconds())
  mediaStream.addTrack(event.track)
  // videoElement.controls.play()
  // log(JSON.stringify(event.streams)+"___"+JSON.stringify(event.track))
  // el = document.createElement('source')
  // el.srcObject = event.streams[0]
  // if(event.track.kind==='video'){
  //   videoElement.srcObject = event.streams[0]
  //   videoElement.videoTracks.push(event.track    )
  //   log(event.streams[0])
  //   let parent = document.getElementById('remoteVideos')
  //   videoElement.muted = false
  //   videoElement.autoplay = true
  //   videoElement.controls = true
  //   videoElement.width = parent.offsetWidth
  //   videoElement.height = videoElement.width*9/16
  //   videoElement.play()
  //   log("srcObject_video"+JSON.stringify(videoElement.srcObject))
  //   log("src_video"+JSON.stringify(videoElement.src))
  // }else{
  //   videoElement.audioTracks.push(event.track)
    // var el = document.createElement(event.track.kind)
    // log(event.streams[0])
    // el.srcObject = event.streams[0]
    // el.muted = false
    // el.autoplay = true
    // el.controls = true
    // videoElement.appendChild(el)
    // log("srcObject_audio"+JSON.stringify(videoElement.srcObject))
    // log("src_audio"+JSON.stringify(videoElement.src))
  // }
}

pc.oniceconnectionstatechange = e => log(pc.iceConnectionState)


async function handleNegotiationNeededEvent() {
  let offer = await pc.createOffer();
  await pc.setLocalDescription(offer);
  console.log()
  getRemoteSdp();
}

$(document).ready(function() {
  $('#' + suuid).addClass('active');
  getCodecInfo();
});


function getCodecInfo() {
  $.get("/codec/" + suuid, 
    function(data) {
      try {
        data2 = JSON.parse(data);
        console.log(`get codec ${suuid}`+data);
        if (data2.length > 1) {
          log('add audio Transceiver')
          pc.addTransceiver('audio', {
            'direction': 'recvonly'
          })
        }
      } catch (e) {
        console.log(e);
      } finally {

        log('add video Transceiver')
        pc.addTransceiver('video', {
          'direction': 'recvonly'
        });
        //send ping becouse PION not handle RTCSessionDescription.close()
        sendChannel = pc.createDataChannel('foo');
        sendChannel.onclose = () => console.log('sendChannel has closed');
        sendChannel.onopen = () => {
          console.log('sendChannel has opened');
          sendChannel.send('ping');
          setInterval(() => {
            sendChannel.send('ping');
          }, 1000)
        }
        sendChannel.onmessage = e => log(`Message from DataChannel '${sendChannel.label}' payload '${e.data}'`);
      }
    }
  );
}

let sendChannel = null;

function getRemoteSdp() {
  data2 = btoa(pc.localDescription.sdp);
  console.log(`getRemoteSdp${data2}\n ${pc.localDescription.sdp}`)
  $.post("/recive", {
    suuid: suuid,
    data: btoa(pc.localDescription.sdp)
  }, function(data) {
    try {
      console.log(`getRemoteSdp ${data}`)
      pc.setRemoteDescription(new RTCSessionDescription({
        type: 'answer',
        sdp: atob(data)
      }))



    } catch (e) {
      console.warn(e);
    }

  });
}