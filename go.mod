module github.com/deepch/RTSPtoWebRTC

go 1.14

require (
	github.com/deepch/vdk v0.0.0-20200811012133-292592fcb5e6
	github.com/gin-gonic/gin v1.6.3
	github.com/pion/webrtc/v2 v2.2.23
	golang.org/x/mobile v0.0.0-20200801112145-973feb4309de // indirect
)
