package main

import (
	"log"
	"os"
	"os/exec"
	"os/signal"
	"syscall"
)

func main() {
	go serveHTTP()
	go serveStreams()
	cmnd := runWebRtcProcess()
	defer cmnd.Process.Kill()
	sigs := make(chan os.Signal, 1)
	done := make(chan bool, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		sig := <-sigs
		log.Println(sig)

		done <- true
	}()
	log.Println("Server Start Awaiting Signal")
	<-done
	log.Println("Exiting")
}
func runWebRtcProcess() *exec.Cmd {
	cmnd := exec.Command("webrtc.exe", "-H8000")
	// cmnd.Run()
	cmnd.Start()
	return cmnd
}
