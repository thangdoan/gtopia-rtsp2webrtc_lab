package main

import (
	"log"
	"time"

	"github.com/deepch/vdk/format/rtsp"
	"github.com/deepch/vdk/format/rtspv2"
)

func serveStreamsV2() {
	for k, v := range Config.Streams {
		go func(name, url string) {
			for {
				var options = rtspv2.RTSPClientOptions{
					Debug:            true,
					URL:              url,
					DialTimeout:      30 * time.Second,
					ReadWriteTimeout: 5 * time.Second,
					DisableAudio:     false,
				}
				log.Println(name, "connect", url)

				session, err := rtspv2.Dial(options)
				if err != nil {
					log.Println(name, err)
					time.Sleep(5 * time.Second)
					continue
				}
				Config.coAd(name, session.CodecData)
				for {
					pkt := <-session.OutgoingPacket
					if err != nil {
						log.Println(name, err)
						break
					}
					Config.cast(name, *pkt)
				}
				// if err != nil {
				// 	log.Println("session Close error", err)
				// }
				log.Println(name, "reconnect wait 5s")
				time.Sleep(5 * time.Second)
			}
		}(k, v.URL)
	}
}

func serveStreams() {
	tag := "stream.GO"
	for k, v := range Config.Streams {
		go func(name, url string) {
			for {
				log.Println(name, "connect", url)
				rtsp.DebugRtsp = true
				session, err := rtsp.Dial(url)
				if err != nil {
					log.Println(name, err)
					time.Sleep(5 * time.Second)
					continue
				}
				session.RtpKeepAliveTimeout = 30 * time.Second

				codec, err := session.Streams()
				if err != nil {
					log.Println(name, err)
					time.Sleep(5 * time.Second)
					continue
				}
				Config.coAd(name, codec)
				for {
					pkt, err := session.ReadPacket()
					if err != nil {
						log.Println(tag, name, err)
						break
					}
					Config.cast(name, pkt)

				}
				err = session.Close()
				if err != nil {
					log.Println(tag, "session Close error", err)
				}
				log.Println(tag, name, "reconnect wait 5s")
				time.Sleep(5 * time.Second)
			}
		}(k, v.URL)
	}
}
func goConnectStream(name, url string) {
	tag := "stream.GO"
	go func(name, url string) {
		for {
			log.Println(name, "connect", url)
			rtsp.DebugRtsp = true
			session, err := rtsp.Dial(url)
			if err != nil {
				log.Println(name, err)
				time.Sleep(5 * time.Second)
				continue
			}
			session.RtpKeepAliveTimeout = 30 * time.Second

			codec, err := session.Streams()
			if err != nil {
				log.Println(name, err)
				time.Sleep(5 * time.Second)
				continue
			}
			Config.coAd(name, codec)
			for {
				pkt, err := session.ReadPacket()
				if err != nil {
					log.Println(tag, name, err)
					break
				}
				Config.cast(name, pkt)

			}
			err = session.Close()
			if err != nil {
				log.Println(tag, "session Close error", err)
			}
			log.Println(tag, name, "reconnect wait 5s")
			time.Sleep(5 * time.Second)
		}
	}(name, url)
}

// ,
//     "cam1": {
//       "url": "rtsp://admin:gtopialab123@10.0.0.199/cam/realmonitor?channel=1&subtype=00"
//     },
//     "cam3": {
//       "url": "rtsp://admin:gtopialab123@10.0.0.199/cam/realmonitor?channel=3&subtype=00"
//     },
//     "cam4": {
//       "url": "rtsp://admin:gtopialab123@10.0.0.199/cam/realmonitor?channel=4&subtype=00"
//     }
